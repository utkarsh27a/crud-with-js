var users = [];

function fillUsers(users = []) {
    let html = '';
    for (var i = 0; i < users.length; i++) { 
        let sno = i + 1;
        let name = users[i].name;
        let email = users[i].email;
        let password = users[i].password;
        html += '<tr sno="'+ sno +'">';
            html += '<th scope="row">'+ sno +'</th>';
            html += '<td>'+ (name) +'</td>';
            html += '<td>'+ (email) +'</td>';
            html += '<td>'+ (password) +'</td>';
            html += '<td> <button onclick="showEditUserModal(\''+ sno +'\',\''+ (name) +'\',\''+ (email) +'\',\''+ (password) +'\')"> Edit </button> </td>';
        html += '</tr>';
    }
    $("#user-list tbody").html(html);
}

function addUser(name, email, password) {
    users.push({ name, email, password });
    fillUsers(users);
    $("#pills-user-list-tab").click();
}

function submitAddUserForm(event) {
    event.preventDefault();
    let name = $('#add-user-form #name').val();
    let email = $('#add-user-form #email').val();
    let password = $('#add-user-form #password').val();
    addUser(name, email, password);
}

function showEditUserModal(sno, name, email, password) {
    $('#edit-user-modal #sno').val(sno);
    $('#update-user-form #name').val(name);
    $('#update-user-form #email').val(email);
    $('#update-user-form #password').val(password);
    $('#edit-user-modal').modal('show');
}

function submitUpdateUserForm(event) {
    event.preventDefault();
    let sno = $('#update-user-form #sno').val();
    let name = $('#update-user-form #name').val();
    let email = $('#update-user-form #email').val();
    let password = $('#update-user-form #password').val();
    updateUser(sno, name, email, password);
}

function updateUser(sno, name, email, password) {
    $('#user-list tbody tr[sno="'+sno+'"] td:eq(0)').text(name);
    $('#user-list tbody tr[sno="'+sno+'"] td:eq(1)').text(email);
    $('#user-list tbody tr[sno="'+sno+'"] td:eq(2)').text(password);
    $('#edit-user-modal').modal('hide');
}

$(function() {
    fillUsers(users);
});
